# Zeptun


Egy diákok álltal használt tanulmányokat kezelő program kerül fejlesztésre, regisztrációt követően a tanulónak lehetősége van felvenni tantárgyakat, beírni a hozzá tartózo érdemjegyet. Lekérdezni az aktuállisan megszerzett krediteit, beírt jegyek álltal számolt kerditel súlyozott áltagát.


**Funkciók:**

    
- _**Regisztráció:**_ A Diákok tudják magukat a rendszerben. Egy egyedi Zeptunk lesz a felhasználó nevük amit ők választhatnak meg. Regisztráció során csak a Zeptunkód, Vezetéknév, Keresztnév, és a jelszó megadása kötelező.


- _**Tantárgy felvétel:**_  Egy előlre elkészitett tantárgyakat tartalmazó listából lehetősége van a tanulónak felvenni tárgyakat.

- _**Jegy beírás:**_ A felvett tantárgyak megszerzett érdemjegyét tudják beírni.

- _**Személyes adatok:**_ Egy oldal ahol megjelenitésre kerülnek a személyes adataok, plusz az eddig elért tanulmányi átlag kredittel súlyozott, az eddig összes felvett kredit és a diplomáig hátralévő kreditek száma. (A diplomáig hátralévő kreditek számát a összes Tantárgy kredit összegéből kivonja a már teljesitett krediteket)


